from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, Sale, Salesperson, Customer
from .encoder import AutomobileVOEncoder, SaleEncoder, SalesPersonEncoder, CustomerEncoder

# Create your views here.

@require_http_methods(["GET"])
def api_automobile_vo_list(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder

        )


@require_http_methods(["GET", "POST"])
def api_sales_list(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            id = content['salesperson']
            salesperson = Salesperson.objects.get(employee_id=id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"},
                status=400,
            )
        try:
            id = content["customer"]
            customer = Customer.objects.get(id=id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            automobile.sold = True
            automobile.save()
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile VIN doesn't exist"},
                status=400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, pk):

    if request.method == "DELETE":
        try:
            count, _ = Sale.objects.get(id=pk).delete()
            return JsonResponse(
                {"message": count > 0}
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale not found"},
                status=400
            )



@require_http_methods(["GET", "POST"])
def api_customer_list(request):

    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers":customers},
            encoder=CustomerEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            CustomerEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Customer.objects.get(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer not found"},
                status=400,
            )



@require_http_methods(["GET", "POST"])
def api_salespeople_list(request):

    if request.method == "GET":
        salespersons = Salesperson.objects.all()

        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalesPersonEncoder,
            safe=False)
    else:
        content = json.loads(request.body)

        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_salesperson(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Salesperson.objects.get(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0 }
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson not found"},
                status=400,
            )
