from django.urls import path
from .views import (
    api_automobile_vo_list,
    api_customer_list,
    api_sales_list,
    api_salespeople_list,
    api_delete_customer,
    api_delete_salesperson,
    api_delete_sale



)


urlpatterns = [
    path("automobiles/", api_automobile_vo_list, name="api_automobile_vo_list"),
    path("salespeople/", api_salespeople_list, name="api_salespeople_list"),
    path("salespeople/<int:id>/", api_delete_salesperson, name="api_show_salesperson"),
    path("customers/", api_customer_list, name="api_customer_list"),
    path("customers/<int:id>/", api_delete_customer, name="api_show_customer"),
    path("sales/", api_sales_list, name="api_sales_list"),
    path("sales/<int:id>/", api_delete_sale, name="api_show_sale"),
]
