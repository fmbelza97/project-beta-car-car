import React, { useState, useEffect } from 'react';

export default function SalesHistory() {

    const [salesperson, setSalesperson] =useState('');

    const [salespersons, setSalespersons] = useState([]);

    useEffect(() => {
        const fetchSalesperson = async () => {
            const response = await fetch('http://localhost:8090/api/salespeople/');
            if (response.ok) {
                const data = await response.json();
                setSalespersons(data.salespersons);
            }
        };

        fetchSalesperson();
    }, []);



    const [sales, setSales] = useState([]);

    const fetchSales = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl);
        if (response.ok) {
            const data = await response.json();
            const filteredSales = data.sales.filter(sale => sale.salesperson.employee_id === parseInt(salesperson));
            setSales(filteredSales)
        }
    }


    useEffect(() => {
        fetchSales()
    }, [salesperson]);



    function handleSalespersonChange(e) {
        const value = e.target.value;
        setSalesperson(value);
    }




    return (
        <>
            <div className="container">
                <h1>Sales History </h1>
                <div className="row">
                    <form id="form-search" name="formsearch" method="get" action="" className="form-inline">
                        <div className="form-group">
                            <div className="select-group">
                            <select onChange={handleSalespersonChange} value={salesperson} required className="form-select" id="salesperson">
                                <option value="">Choose a salesperson...</option>
                                {salespersons.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                                )
                                })}
                            </select>
                            </div>
                        </div>
                    </form>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer Name</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.map((sales) => {
                                return (
                                    <tr key={sales.id}>
                                        <td>{sales.salesperson.first_name} {sales.salesperson.last_name}</td>
                                        <td>{sales.customer.first_name} {sales.customer.last_name}</td>
                                        <td>{sales.automobile.vin}</td>
                                        <td>${sales.price}</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </div>
        </>
    )
}
