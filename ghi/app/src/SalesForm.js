import React, { useState, useEffect } from 'react';


function SalesForm() {

    const[formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
    })


    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {
        const fetchAutomobiles = async () => {
            const response = await fetch('http://localhost:8090/api/automobiles/');
            if (response.ok) {
                const data = await response.json();
                setAutomobiles(data.automobiles);
            }
        };

        fetchAutomobiles();
    }, []);



    const [salespersons, setSalespersons] = useState([]);

    useEffect(() => {
        const fetchSalespersons = async () => {
            const response = await fetch('http://localhost:8090/api/salespeople/');
            if (response.ok) {
                const data = await response.json();
                setSalespersons(data.salespersons);
            }
        };

        fetchSalespersons();
    }, []);



    const [customers, setCustomers] = useState([]);

    useEffect(() => {
        const fetchCustomers = async () => {
            const response = await fetch('http://localhost:8090/api/customers/');
            if (response.ok) {
                const data = await response.json();
                setCustomers(data.customers);
            }
        };

        fetchCustomers();
    }, []);



    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = 'http://localhost:8090/api/sales/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salesUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                automobile: '',
                salesperson: '',
                customer: '',
                price: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;


        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="create-sales-form">
                    <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.automobile} required name="automobile" id="automobile"  className="form-select">
                                <option value="">Choose an automobile VIN...</option>
                                {automobiles.map(auto => {
                                return (
                                <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                                );
                            })}
                            </select>
                    </div>
                    <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.salesperson} required name="salesperson" id="salesperson"  className="form-select">
                                <option value="">Choose a salesperson...</option>
                                {salespersons.map(salesperson => {
                                return (
                                <option key={salesperson.id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                                );
                            })}
                            </select>
                    </div>
                    <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.customer} required name="customer" id="customer"  className="form-select">
                                <option value="">Choose a customer...</option>
                                {customers.map(customer => {
                                return (
                                <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                );
                            })}
                            </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.price} placeholder="price" required type="text" name="price" id="price" className="form-control" />
                        <label htmlFor="price">Price</label>
                    </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    );
}

export default SalesForm;
