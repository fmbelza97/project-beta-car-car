import React, { useState, useEffect } from 'react';

function AutomobileForm() {
  const [formData, setFormData] = useState({
    vin: '',
    color: '',
    year: '',
    model_id: '',
  });

  const [models, setModels] = useState([]);

  const fetchModels = async () => {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchModels();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    await fetch(automobileUrl, fetchConfig);

    setFormData({
      vin: '',
      color: '',
      year: '',
      model_id: '',
    });
  };

  const handleFormChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a New Automobile</h1>
          <form onSubmit={handleSubmit} id="add-automobile-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.vin}
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.year}
                placeholder="Year"
                required
                type="number"
                name="year"
                id="year"
                className="form-control"
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.model_id}
                required
                name="model_id"
                id="model_id"
                className="form-select"
              >
                <option value="">Choose a model</option>
                {models.map((model) => (
                  <option key={model.id} value={model.id}>
                    {model.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
