import React, { useState, useEffect } from 'react';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    const fetchAppointments = async () => {
      const response = await fetch('http://localhost:8080/api/appointments/');
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
      }
    };

    fetchAppointments();
  }, []);

  const handleCancelAppointment = async (id) => {
    const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
      method: 'PUT',
    });

    if (response.ok) {
      setAppointments((prevAppointments) =>
        prevAppointments.filter((appointment) => appointment.id !== id)
      );
    }
  };

  const handleFinishAppointment = async (id) => {
    const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
      method: 'PUT',
    });

    if (response.ok) {
      setAppointments((prevAppointments) =>
        prevAppointments.filter((appointment) => appointment.id !== id)
      );
    }
  };

  return (
    <div>
      <h1>List of Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date and Time</th>
            <th>Technician</th>
            <th>Reason for Service</th>
            <th>VIP</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.vip ? 'Yes' : 'No'}</td>
                <td>
                  <button type="button" className="btn btn-danger" onClick={() => handleCancelAppointment(appointment.id)}>
                    Cancel
                  </button>
                  <button type="button" className="btn btn-success" onClick={() => handleFinishAppointment(appointment.id)}>
                    Finish
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
